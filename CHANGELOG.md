
# Changelog for RStudio Wrapper Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.5.0] - 2021-04-21

-  RStudio instance allocation takes into account the number of instance available in the VO and not in the context and assign the RStudio instance balancing the load. 

- The user is always redirected to the same endpoint, until a gCore Endpoint disappears, if so a new allocation is calculated.

## [v1.3.1] - 2019-10-07

- RStudio instances returned over https now


## [v1.3.0] -2017-02-21

- RStudio instance allocation takes into account the number of instance available in the given context and assign the RStudio instance balancing the load.

- Added possibility to look for service endpoint RConnector resource in the scope
	
## [v1.0.0] -2016-04-11

- First release
